/*
 * circ_queue.h
 *
 *  Created on: Sep 1, 2016
 *      Author: pooshkar.r
 */

#ifndef CBS_SHARED_PARAM_API_CIRC_QUEUE_H_
#define CBS_SHARED_PARAM_API_CIRC_QUEUE_H_

/*==============================================================================

                               INCLUDE FILES

==============================================================================*/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "stdbool.h"
#include "stdint.h"
#include "typedefs.h"

/*==============================================================================

                      DEFINITIONS AND TYPES : MACROS

==============================================================================*/
#define MAX_DATA_ITEMS  1024







/*==============================================================================

                      DEFINITIONS AND TYPES : ENUMS

==============================================================================*/







/*==============================================================================

                   DEFINITIONS AND TYPES : STRUCTURES

==============================================================================*/

typedef struct
{
    int     head;
    int     tail;
    uint32_t validItems;
    uint8  data[MAX_DATA_ITEMS];
} circularQueue_s;



/*==============================================================================

                           EXTERNAL DECLARATIONS

==============================================================================*/

extern circularQueue_s *circ_queue;

extern uint32_t isQueueFilled;

/*==============================================================================

                           FUNCTION PROTOTYPES

==============================================================================*/

/*----------------------------------------------------------------------------*/

void initializeQueue(circularQueue_s *theQueue);

int isEmpty(circularQueue_s *theQueue);

int8_t isFull (circularQueue_s *theQueue);

int32_t putItem(circularQueue_s *theQueue, uint8 theItemValue);

int32_t getItem(circularQueue_s *theQueue, uint8 *theItemValue);
















#endif /* CBS_SHARED_PARAM_API_CIRC_QUEUE_H_ */
