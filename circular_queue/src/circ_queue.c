/*
 * circ_queue.c
 *
 *  Created on: Sep 1, 2016
 *      Author: pooshkar.r
 */
// Test Change Jenkins
// Test2 Change Jenkins
// Test3 Change Jenkins- Webhook enabled
#include "stdbool.h"
#include "circ_queue.h"
#include "stdint.h"
#include "stdbool.h"

/*----------------------------------------------------------------------------
 Functions for simple queue operations
----------------------------------------------------------------------------*/



void initializeQueue(circularQueue_s *theQueue)
{
	memset(theQueue, 0, sizeof(circularQueue_s));
	return;
}



int isEmpty(circularQueue_s *theQueue)
{
	if(theQueue->validItems==0)
	return(1);
	else
	return(0);
}

int8_t isFull (circularQueue_s *theQueue)
{
	if((theQueue->validItems== MAX_DATA_ITEMS))
	{
		return (true);
	}
	else
	{
		return (false);
	}
}


int32_t putItem(circularQueue_s *theQueue, uint8 theItemValue)
{
	if((theQueue->validItems>=MAX_DATA_ITEMS))
	{
		UARTprintf("The queue is full\n");
		UARTprintf("You cannot add items\n");
		return(-1);
	}
	else
	{
		theQueue->validItems++;
		theQueue->data[theQueue->tail] = theItemValue;
		theQueue->tail = (theQueue->tail+1)%MAX_DATA_ITEMS;
//		if( theQueue->head == theQueue->tail )
//		{
//			isQueueFilled = 1;
//			UARTprintf("\n The circular buffer is filler, you cannot add more items");
//			return (1);
//		}
		return 0;
	}
}



int32_t getItem(circularQueue_s *theQueue, uint8 *theItemValue)
{
	if(isEmpty(theQueue))
	{
		UARTprintf("isempty\n");
		return(-1);
	}
	else
	{
		*theItemValue=theQueue->data[theQueue->head];
		theQueue->head=(theQueue->head+1)%MAX_DATA_ITEMS;
		theQueue->validItems--;
		return(0);
	}
}

